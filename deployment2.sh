#!/bin/bash

set -o errexit

#===================
## Variables
username_to_create="eric" #No uppercase or special characters here
pwd_for_new_user="123"  #Provide stronk password
#===================


#=======================
## DO NOT TOUCH THIS   /
#######################
Bl="\e[5m"
CB="\e[46m\e[30m"
RW="\e[41m\e[97m"
R="\e[0m"
u=$USER
etc=$PWD/config/etc
dot=$PWD/config/dotfiles
f2b=$PWD/config/fail2ban
ps=$PWD/config/portsentry
web=$PWD/config/web
#=======================

## Function to display dots and sleep
dots() {
    for i in {0..2}; do
	echo -n ". "
	sleep 0.3s
    done
    echo ""
}

## Verify if root
if [[ $u != "root" ]]; then
    echo -e "Not root!\n"
    echo "Usage:"
    echo -e "\tsudo ./`echo $(basename $0)`\t(no reboot)"
    echo -e "or\tsudo ./`echo $(basename $0)` -r\t(to reboot after script finishes)"
    exit 0
fi

## Verify if variables
if [[ -z $username_to_create || -z $pwd_for_new_user ]]; then
    echo "Script variables aren't set!"
    exit 1
else
    un=$username_to_create
    pw=$pwd_for_new_user
    home=/home/$un
fi

echo -en "${CB}Running full system update$R " ; dots
echo \(Be patient!\)
[ -f /etc/resolv.conf ] && : || echo 'nameserver 10.51.1.5' > /etc/resolv.conf
apt-get update									> /dev/null 2>&1
apt-get -y dist-upgrade								> /dev/null 2>&1
echo -e 'Done.\n'

echo -en "${CB}Upgrading packages$R " ; dots
echo \(Be patient!\)
sleep 2s
tmp=$(sudo apt list 2>/dev/null | grep portsentry/ | grep -c installed) || true
apt-get upgrade -y 								> /dev/null 2>&1
if [ $? -lt "1" ]; then
    export DEBIAN_FRONTEND=noninteractive
    apt-get install -y portsentry	> /dev/null 2>&1
fi
systemctl stop apache2			> /dev/null 2>&1
apt-get install -y curl git zsh ufw	\
	fail2ban nginx-full auditd ssh	\
	portsentry net-tools mailutils						> /dev/null 2>&1
apt-get autoremove -y 								> /dev/null 2>&1
auditctl -w /etc/crontab -p wa -k monitor-hosts -i				> /dev/null 2>&1
systemctl enable auditd								> /dev/null 2>&1
systemctl restart auditd							> /dev/null 2>&1
echo -e 'Done.\n'

echo -en "${CB}Preparing network config$R " ; dots
rm -f /etc/network/interfaces.d/enp0s3  /etc/network/interfaces.d/enp0s8
cp $etc/enp0s3 $etc/enp0s8 /etc/network/interfaces.d
touch /etc/resolv.conf
mv /etc/resolv.conf /etc/resolv.conf.bak
cp $etc/resolv.conf /etc
systemctl restart networking
echo -e 'Done.\n'

echo -en "${CB}Preparing sshd config$R " ; dots
touch /etc/ssh/sshd_config
mv /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
cp $etc/sshd_config /etc/ssh
systemctl enable ssh								> /dev/null 2>&1
systemctl restart ssh								> /dev/null 2>&1
echo -e 'Done.\n'

echo -en "${CB}Disabling unused services$R " ; dots
systemctl disable lightdm		> /dev/null 2>&1
update-rc.d -f alsa-utils	remove
update-rc.d -f anacron		remove
update-rc.d -f bluetooth	remove
update-rc.d -f cups		remove
update-rc.d -f cups-browsed	remove
update-rc.d -f dbus		remove
update-rc.d -f hddtemp		remove
update-rc.d -f kmod		remove
update-rc.d -f lightdm		remove
update-rc.d -f lm-sensors	remove
update-rc.d -f network-manager	remove
update-rc.d -f x11-common	remove
echo -e 'Done.\n'

echo -en "${CB}Preparing oh-my-zsh template for all users$R " ; dots
rm -f /etc/adduser.conf
cp $etc/adduser.conf /etc
rm -f /etc/skel/.bash_logout /etc/skel/.bashrc /etc/skel/.profile
cp $dot/zshrc /etc/skel/.zshrc
echo -e 'Done.\n'

echo -en "${CB}Configuring crontab$R " ; dots
rm -f /root/cron_send_mail	\
   /root/cron_upgrade_all	\
   /var/spool/cron/crontabs/crontab
cp $dot/cron_send_mail /root
cp $dot/cron_upgrade_all /root
crontab -u root $etc/crontab
echo -e 'Done.\n'

echo -en "${CB}Configuring firewall (ufw)$R " ; dots
yes | ufw enable								> /dev/null 2>&1
yes | ufw allow 2222/tcp							> /dev/null 2>&1
yes | ufw allow 'Nginx Full'							> /dev/null 2>&1
echo -e 'Done.\n'

echo -en "${CB}Configuring fail2ban$R " ; dots
rm -f /etc/fail2ban/jail.local				\
      /etc/fail2ban/jail.d/sshd.conf			\
      /etc/fail2ban/jail.d/nginx.conf			\
      /etc/fail2ban/filter.d/nginx-badbots.conf		\
      /etc/fail2ban/filter.d/nginx-http-auth.conf	\
      /etc/fail2ban/filter.d/nginx-nohome.conf		\
      /etc/fail2ban/filter.d/nginx-noscript.conf	\
      /etc/fail2ban/filter.d/nginx-botsearch.conf	\
      /etc/fail2ban/filter.d/nginx-limit-req.conf	\
      /etc/fail2ban/filter.d/nginx-noproxy.conf
cp $f2b/jail.local /etc/fail2ban
cp $f2b/sshd.conf /etc/fail2ban/jail.d
cp $f2b/nginx.conf /etc/fail2ban/jail.d
cp $f2b/filter.d/* /etc/fail2ban/filter.d
systemctl enable fail2ban							> /dev/null 2>&1
systemctl restart fail2ban							> /dev/null 2>&1
echo -e 'Done.\n'

echo -en "${CB}Configuring portsentry$R " ; dots
touch /etc/portsentry/portsentry.conf		\
      /etc/portsentry/portsentry.ignore.static
mv /etc/portsentry/portsentry.conf 		\
   /etc/portsentry/portsentry.conf.bak
mv /etc/portsentry/portsentry.ignore.static	\
   /etc/portsentry/portsentry.ignore.static.bak
cp $ps/portsentry.conf /etc/portsentry/portsentry.conf
cp $ps/portsentry.ignore.static /etc/portsentry/portsentry.ignore.static
systemctl enable portsentry			                                > /dev/null 2>&1
systemctl restart portsentry                  					> /dev/null 2>&1
echo -e 'Done.\n'


echo -en "${CB}Configuring webserver$R " ; dots
rm -rf /var/www/html
mkdir /var/www/html
export TAPE=$web/nginx.tar.gz2
tar xj -C /etc/nginx
cp -r $web/html/* /var/www/html
key=$(find /etc/ssl/private -name nginx-selfsigned.key)
crt=$(find /etc/ssl/certs -name nginx-selfsigned.crt)
dh=$(find /etc/ssl/certs -name dhparam.pem)
if [[ -z $key || -z $crt ]]; then
    openssl req -subj '/CN=./C=FR' -x509			\
	    -nodes -days 365 -newkey rsa:2048			\
	    -keyout /etc/ssl/private/nginx-selfsigned.key	\
	    -out /etc/ssl/certs/nginx-selfsigned.crt				> /dev/null 2>&1
fi
if [ -z $dh ]; then
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048			> /dev/null 2>&1
fi
systemctl enable nginx								> /dev/null 2>&1
systemctl restart nginx								> /dev/null 2>&1
echo -e 'Done.\n'


echo -en "${CB}Creating $Bl$un$R$CB user account$R " ; dots
tmp=$(getent passwd | grep -c $un || true)
if [ $tmp -lt "1" ]; then
    rm -rf $home
    yes "" | adduser $un --disabled-password					> /dev/null 2>&1
    echo -e "$pw\n$pw" | passwd $un						> /dev/null 2>&1
    git clone https://github.com/robbyrussell/oh-my-zsh.git $home/.oh-my-zsh	> /dev/null 2>&1
    chown $un:$un -R $home/.oh-my-zsh
    echo -e 'Done.\n'
else
    base=$(find /home/$un -name .oh-my-zsh)
    if [[ $base == "" ]]; then
	git clone https://github.com/robbyrussell/oh-my-zsh.git $home/.oh-my-zsh	> /dev/null 2>&1
	usermod -s /usr/bin/zsh $un
	cp $dot/zshrc /home/$un/.zshrc
	chown $un:$un -R $home/.oh-my-zsh $home/.zshrc
    fi
    echo -e "${RW}Error: User Account '$un' already exists. :c$R\n"
fi

echo -en "${CB}Creating for $Bl$un$R$CB SSH-keypair of 4096 bytes$R " ; dots
mkdir -p $home/.ssh
ssh_prv=$(find $home/.ssh -name id_rsa)
ssh_pub=$(find $home/.ssh -name id_rsa.pub)
if [[ -z $ssh_prv || -z $ssh_pub ]]; then
    yes | ssh-keygen -b 4096 -f $home/.ssh/id_rsa -q -P ""
    echo -e 'Done.\n'
else
    echo -e "${RW}Error: SSH keypair already exists. Nothing has changed.$R\n"
fi

ac=$#
while [[ $ac -gt "0" ]]; do
    av=$@
    curr=`echo $av | cut -d' ' -f1`
    shift
    ((ac--))
    if [ $curr == '-r' ]; then
        echo "Rebooting in:"
        for i in {5..1}; do
            echo "${i}s"
            sleep 1s
        done
        systemctl reboot
    fi
done
