#!/bin/bash

set -e

## Verify if root
if [[ $USER != "root" ]]; then
    echo -e "Not root!\n"
    echo "Usage:"
    echo -e "\tsudo ./`echo $(basename $0)`"
    exit 0
fi

rm -rf $HOME/rg
git clone https://gitlab.com/eschnell/roger_skyline.git $HOME/rg
tmp=$(mktemp -d)
tmp_file=$tmp/cron
cat config/etc/crontab >> $tmp_file
cat config/etc/crontab_deploy >> $tmp_file
crontab -u root $tmp_file
rm -rf $tmp/
